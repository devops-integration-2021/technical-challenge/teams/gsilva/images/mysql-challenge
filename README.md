//Creación de BD

wget https://gitlab.com/devops-integration-2021/project-info/-/raw/master/mysql-dev.zip

unzip mysql-dev.zip

cd mysql-dev

docker build -t mysql-db:1 .

docker run -it -d -p 4000:3306 -e MYSQL_ROOT_PASSWORD=root --name mysql-challenge mysql-db:1 mysqld --lower_case_table_names=1
