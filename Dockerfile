FROM mysql:5.6

MAINTAINER Gerson Silva <gersilva2206@gmail.com>

COPY 01.-Scripts.sql /docker-entrypoint-initdb.d
